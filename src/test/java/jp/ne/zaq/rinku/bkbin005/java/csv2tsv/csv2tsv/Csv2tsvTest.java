/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.java.csv2tsv.csv2tsv;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Test;

/**
 * @author bkbin
 *
 */
public class Csv2tsvTest extends Csv2tsv {

  Csv2tsv target = new Csv2tsv();

  /**
   * {@link jp.ne.zaq.rinku.bkbin005.java.csv2tsv.csv2tsv.Csv2tsv#main(java.lang.String[])} のためのテスト・メソッド。
   * @throws IOException
   */
  @Test
  public void testMain() throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));
    final String expected = "id\tname\n135\tMitsutoshi NAKANO\n";
    final String[] args = { "src/test/resources/test.csv" };
    main(args);
    assertEquals(expected, baos.toString());
  }

  /**
   * {@link jp.ne.zaq.rinku.bkbin005.java.csv2tsv.csv2tsv.Csv2tsv#convert(java.lang.String)} のためのテスト・メソッド。
   * @throws IOException
   */
  @Test
  public void testConvert() throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));
    final String expected = "id\tname\n135\tMitsutoshi NAKANO\n";
    target.convert("src/test/resources/test.csv");
    assertEquals(expected, baos.toString());
  }

  /**
   * {@link jp.ne.zaq.rinku.bkbin005.java.csv2tsv.csv2tsv.Csv2tsv#putLine(java.lang.String[])} のためのテスト・メソッド。
   */
  @Test
  public void testPutLine() {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));

    final String[] input = { "a", "b" };
    final String expected = "a\tb\n";
    target.putLine(input);
    assertEquals(expected, baos.toString());
  }

}
