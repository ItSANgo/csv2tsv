/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.java.csv2tsv.csv2tsv;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.orangesignal.csv.Csv;
import com.orangesignal.csv.CsvConfig;
import com.orangesignal.csv.handlers.StringArrayListHandler;

/**
 * @author Mitsutoshi NAKANO
 *
 */
public class Csv2tsv {

  /**
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    Csv2tsv instance = new Csv2tsv();
    instance.convert(args[0]);
  }

  void convert(String fname) throws IOException {
    List<String[]> list = Csv.load(new File(fname), new CsvConfig(),
        new StringArrayListHandler());
    for (String[] line : list) {
      putLine(line);
    }
  }

  void putLine(String[] line) {
    for (int i = 0; i < line.length; ++i) {
      System.out.print(line[i]);
      System.out.print((i < line.length - 1) ? "\t" : "\n");
    }
  }

}
